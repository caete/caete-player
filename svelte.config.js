import staticAdapter from '@sveltejs/adapter-static';
import preprocess from "svelte-preprocess";
import autoprefixer from 'autoprefixer';

/** @type {import('@sveltejs/kit').Config} */
const config = {
  kit: {
    adapter: staticAdapter({
      pages: 'public',
      assets: 'public',
      fallback: 'index.html'
    }),
    paths: {
      base: '/caete-player'
    },
    appDir: 'internal'
	},
  preprocess: preprocess({
    postcss: {
      plugins: [autoprefixer]
    },
  }),

};

export default config;