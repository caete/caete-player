export const millisecondsToTimecode = (currentTime: number): string => {
  const timeInSeconds = Math.round(currentTime / 1000)
  const hours = Math.floor(timeInSeconds / 3600)
  const minutes = Math.floor(timeInSeconds % 3600 / 60)
  const seconds = Math.floor(timeInSeconds % 3600 % 60)
  return `${hours < 10 ? '0' + hours : hours}:${minutes < 10 ? '0' + minutes : minutes}:${seconds < 10 ? '0' + seconds : seconds}`
}

export const timecodeToMilliseconds = (timecode: string): number => {
  const time = timecode.split(':')
  const hours = time.length > 2 ? parseInt(time[0]) * 3600 : 0
  const minutes = (time.length > 2 ? parseInt(time[1]) : parseInt(time[0])) * 60
  const seconds = time.length > 2 ? parseInt(time[2]) : parseInt(time[1])
  return  (hours+minutes+seconds)*1000
}