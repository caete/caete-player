export const blockDefaults = {
  Image: {
    "type": "Image",
    "timeInterval": {
      "in": 0,
      "out": 0
    },
    "position": {"x": 35, "y": 40},
    "size": {"width": 30, "height": "auto"},
    "transitions": {
      "in": {
        "duration": 0,
        "translation": {"x": 0, "y": 0},
        "opacity": 100
      },
      "out": {
        "duration": 0,
        "translation": {"x": 0, "y": 0},
        "opacity": 100
      }
    },
    "config": {
      "src": "https://caete.gitlab.io/images/caete_pt.svg",
      "alt": "Logo do Caeté Porreta",
      "style": {}
    }
  },
  Text: {
    "type": "Text",
    "timeInterval": {
      "in": 0,
      "out": 0
    },
    "position": {"x": 0, "y": 0},
    "size": {"width": "auto", "height": "auto"},
    "transitions": {
      "in": {
        "duration": 0,
        "translation": {"x": 0, "y": 0},
        "opacity": 100
      },
      "out": {
        "duration": 0,
        "translation": {"x": 0, "y": 0},
        "opacity": 100
      }
    },
    "config": {
      "text": "Caeté Porreta",
      "style": {}
    }
  }
}

export const getBlockDefaults = (type: BlockTypes, time: number) => {
  const thisDefaults = JSON.parse(JSON.stringify(blockDefaults[type]))
  thisDefaults.timeInterval.in = time
  thisDefaults.timeInterval.out = time + 3000
  return thisDefaults
}