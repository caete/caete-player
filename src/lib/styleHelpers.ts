export const applyCSS = (styles: Style) => {
  if (typeof styles === 'undefined') return ''

  let currentStyles = ''
  Object.keys(styles).forEach(style => {
    currentStyles += `${style}: ${styles[style]}${buttonConfig[style].unit}; `
  })
  return currentStyles
}


import Duration from 'svelte-icons-pack/cg/CgTimelapse'
import Ratio from 'svelte-icons-pack/ri/RiMediaAspectRatioLine'
import StartTime from 'svelte-icons-pack/cg/CgTimer'
import Transition from 'svelte-icons-pack/bi/BiTimer'

import Color from 'svelte-icons-pack/ri/RiDesignPaletteLine'
import BackgroundColor from 'svelte-icons-pack/ri/RiDesignPaintFill'
import BorderWidth from 'svelte-icons-pack/ai/AiOutlineBorderOuter'
import BorderRadius from 'svelte-icons-pack/ri/RiEditorRoundedCorner'
import Padding from 'svelte-icons-pack/ai/AiOutlineGroup'
import FontSize from "svelte-icons-pack/ai/AiOutlineFontSize"
import Bold from "svelte-icons-pack/im/ImBold"
import Italic from "svelte-icons-pack/im/ImItalic"
import Underline from "svelte-icons-pack/im/ImUnderline"
// import FontColor from "svelte-icons-pack/ri/RiEditorFontColor"
import AlignLeft from "svelte-icons-pack/ri/RiEditorAlignLeft"
import AlignMiddle from "svelte-icons-pack/ri/RiEditorAlignCenter"
import AlignRight from "svelte-icons-pack/ri/RiEditorAlignRight"

import TextEdit from "svelte-icons-pack/bs/BsCursorText"
import LinkEdit from "svelte-icons-pack/hi/HiSolidLink"

export const buttonConfig = {
  'ratio': {
    default: 1.77,
    unit: '',
    icon: Ratio
  },
  'duration': {
    default: 0,
    unit: '',
    icon: Duration
  },
  'start': {
    default: 0,
    unit: '',
    icon: StartTime
  },
  'transition': {
    default: 0,
    unit: '',
    icon: Transition
  },
  'color': {
    default: 'hsla(0,100%,100%,100%)',
    unit: '',
    icon: Color
  },
  'background-color': {
    default: 'hsla(0,0%,0%,100%)',
    unit: '',
    icon: BackgroundColor
  },
  'border-width': {
    default: 0,
    unit: 'em',
    icon: BorderWidth
  },
  'border-radius': {
    default: 0,
    unit: 'em',
    icon: BorderRadius
  },
  'font-size': {
    default: '1',
    unit: 'em',
    icon: FontSize
  },
  'font-weight': {
    default: 'normal',
    alternate: 'bold',
    unit: '',
    icon: Bold
  },
  'font-style': {
    default: 'normal',
    alternate: 'italic',
    unit: '',
    icon: Italic
  },
  'padding': {
    default: 0,
    unit: 'em',
    icon: Padding
  },
  'text-decoration': {
    default: 'none',
    alternate: 'underline',
    unit: '',
    icon: Underline
  },
  'text-align': {
    default: 'left',
    unit: '',
    alternate: ['left', 'center', 'right'],
    icon: [AlignLeft, AlignMiddle, AlignRight]
  },
  'text': {
    default: '',
    unit: '',
    icon: TextEdit
  },
  'src': {
    default: '',
    unit: '',
    icon: LinkEdit
  }
}

export const buttons = {
  player: {
    time: ['duration'],
    style: ['color',
      'background-color',
      'border-width',
      'border-radius'
    ]
  },
  block: {
    time: ['duration', 'start'],
    style: [
    'color',
    'background-color',
    'border-width',
    'border-radius',
    'padding'
    ]
  },
  Text: {
    default: ['text'],
    style: [
      'color',
      'font-size',
      'font-weight',
      'font-style',
      'text-decoration',
      'text-align'
    ]
  },
  Image: {
    default: ['src'],
    style: []
  },
  Video: {
    default: ['src'],
    style: []
  }
}