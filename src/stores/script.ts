  import { writable, } from 'svelte/store'

  const createScript = () => {
    const { subscribe, set, update } =  writable<Script>()

    const deleteBlock = (block: number) => update(script => {
      const previousElements = script.blocks.slice(0, block )
      const followingElements = script.blocks.slice(block + 1)

      script.blocks = previousElements.concat(followingElements)
      script = script
      return script
    })

    const moveBlock = (block: number, position: Position) => update(script => {
      script.blocks[block].position = position
      return script
    })

    const resizeBlock = (block: number, size: Size) => update(script => {
      script.blocks[block].size = size
      return script
    })

    const updateSettings = (settings: Settings) => update(script => {
      script.settings = settings
      return script
    })

    const updateBlock = (block: number, value: Block) => update(script => {
      script.blocks[block] = value
      return script
    })

    const createBlock = (block: Block) => update(script => {
      script.blocks.push(block)
      return script
    })

    return {
      subscribe,
      populate: (script: Script) => set(script),
      deleteBlock,
      moveBlock,
      resizeBlock,
      updateSettings,
      updateBlock,
      createBlock
    }
  }

  export const script = createScript()