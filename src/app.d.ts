// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
// and what to do when importing types
declare namespace App {
	// interface Error {}
	// interface Locals {}
	// interface PageData {}
	// interface Platform {}
}

type BlockTypes = 'Image' | 'Text' | 'Video'
type Position = {x: number, y: number}
type Size = {width: number | 'auto', height: number | 'auto'}
type Directions = {vertical: string, horizontal: string}
type TimeInterval = {in: number, out: number}
type Style = {[key: string]: string}

interface Script {
  settings: Settings
  controls: Controls
  blocks: Block[]
}

interface Settings {
  duration: number
  ratio: number
  config: Config
}

interface Controls {
  color?: string
  background?: string
  rounded?: boolean
}

interface Block {
  type: blockTypes
  timeInterval: TimeInterval
  transitions: BlockTransitions
  size: Size
  crop?: [number, number, number, number]
  position: Position
  config: TextConfig | MediaConfig
}

interface Config {
  style: Style
}

interface BlockTransitions {
  in: Transition
  out: Transition
}

interface Transition {
  duration: number
  translation: Position
  opacity: number
}


interface TextConfig extends Config {
  text: string
}

interface MediaConfig extends Config {
  fit?: 'cover' | 'contain'
  src: string
}

interface ImageConfig extends MediaConfig {
  alt: string
}

interface currentTime {
  duration: number
  percent: number
  seconds: number
}

interface StyleSetting {
  style: string
  default: string | number
  alternate?: string
  unit: string
}

// helpers

interface AxisHelper {
  x: ['clientX', 'clientWidth']
  y: ['clientY', 'clientHeight']
}